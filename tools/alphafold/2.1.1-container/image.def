Bootstrap: docker
From: nvidia/cuda:11.1-cudnn8-runtime-ubuntu18.04
Stage: spython-base

%labels
maintainer="julien.rey@univ-paris-diderot.fr"
contributors="sjoerd.de-vries@inserm.fr"
%post

apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
build-essential \
cmake \
cuda-command-line-tools-11-1 \
git \
hmmer \
kalign \
tzdata \
wget \
&& rm -rf /var/lib/apt/lists/*

# Install HHsuite.
wget -q -P /tmp \
https://github.com/soedinglab/hh-suite/releases/download/v3.3.0/hhsuite-3.3.0-AVX2-Linux.tar.gz \
&& mkdir /opt/hhsuite \
&& tar xvfz /tmp/hhsuite-3.3.0-AVX2-Linux.tar.gz -C /opt/hhsuite/ \
&& ln -s /opt/hhsuite/bin/* /usr/bin \
&& rm /tmp/hhsuite-3.3.0-AVX2-Linux.tar.gz

# Install Miniconda package manager.
wget -q -P /tmp \
https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
&& bash /tmp/Miniconda3-latest-Linux-x86_64.sh -b -p /opt/conda \
&& rm /tmp/Miniconda3-latest-Linux-x86_64.sh

# Install conda packages.
PATH="/opt/conda/bin:$PATH"
conda update -qy conda \
&& conda install -y -c conda-forge \
openmm=7.5.1 \
cudatoolkit==11.5 \
pdbfixer \
pip \
python=3.7

git clone --branch 2.1.1-rpbs https://github.com/sjdv1982/alphafold.git /app/alphafold
wget -q -P /app/alphafold/alphafold/common/ \
https://git.scicore.unibas.ch/schwede/openstructure/-/raw/7102c63615b64735c4941278d92b554ec94415f8/modules/mol/alg/src/stereo_chemical_props.txt

# Install pip packages.
pip3 install --upgrade pip \
&& pip3 install -r /app/alphafold/requirements.txt \
&& pip3 install --upgrade jax jaxlib>=0.1.69+cuda111 -f \
https://storage.googleapis.com/jax-releases/jax_releases.html

# Apply OpenMM patch.
cd /opt/conda/lib/python3.7/site-packages
patch -p0 < /app/alphafold/docker/openmm.patch

# We need to run `ldconfig` first to ensure GPUs are visible, due to some quirk
# with Debian. See https://github.com/NVIDIA/nvidia-docker/issues/1399 for
# details.
# ENTRYPOINT does not support easily running multiple commands, so instead we
# write a shell script to wrap them up.
cd /app/alphafold
echo '#!/bin/bash\n\
python /app/alphafold/run_alphafold.py $@' > /app/run_alphafold.sh \
&& chmod +x /app/run_alphafold.sh
%environment
export PATH="/opt/conda/bin:$PATH"
%runscript
cd /app/alphafold
exec /app/run_alphafold.sh "$@"
%startscript
cd /app/alphafold
exec /app/run_alphafold.sh "$@"
